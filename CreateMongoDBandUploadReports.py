import os
import shutil
import pymongo
import subprocess
from ComputeHashesOfSamples import ComputeHashesOfSamples
from QueryMongoDB import QueryMongoDB
from ClassifyDatabase import ClassifyDatabase
from CreateClassificationCollection import CreateClassificationCollection


class CreateMongoDBandUploadReports:


    # databaseName: the name of the database that you want to create
    # collectionName : the name of the collection of the database
    # sampleReporttDirectory : the relative path of the directory which has json report files
    def __init__(self, databaseName, collectionName, sampleReporttDirectory):
        self.databaseName = databaseName
        self.collectionName = collectionName
        self.myClient = None
        self.myDatabase = None
        self.myCollection = None
        self.sampleReporttDirectory = sampleReporttDirectory
        self.uploadReportList = []
        self.hashFile = ComputeHashesOfSamples(self.sampleReporttDirectory, self.databaseName+"_"+self.collectionName+"_hash.json")
        self.hashFile.getSampleHashes()
        self.queryClassToMakeKeys = QueryMongoDB(self.databaseName, self.collectionName, self.sampleReporttDirectory)
        self.sortedNamesFileName = self.databaseName + "_" + self.collectionName + "_names_sorted.json"
        self.sortedNamesFile = None
        self.classifyDatabase = None
        self.clssificationCollection = None


    # connect to the database, the collection
    # insert json fiels to the database
    def connect(self):
        self.myClient = pymongo.MongoClient("mongodb://localhost:27017/")
        self.myDatabase = self.myClient[self.databaseName]
        self.myCollection = self.myDatabase[self.collectionName]


    def createClassificationCollection(self):
        self.clssificationCollection = CreateClassificationCollection(self.databaseName, self.collectionName, self.sampleReporttDirectory)
        self.clssificationCollection.connect()
        self.clssificationCollection.getPossibleKeyword()
        self.clssificationCollection.classifyHash()
        self.clssificationCollection.inpuDocumentToCC()


    def getSortedNamesFile(self):
        self.classifyDatabase = ClassifyDatabase(self.databaseName, self.collectionName, self.sampleReporttDirectory)
        self.classifyDatabase.connect()
        self.classifyDatabase.getScanResultKeys()
        self.classifyDatabase.getMalwareName()
        self.classifyDatabase.sortMalwareName()
        self.classifyDatabase.saveSortedNames()
        self.sortedNamesFile = open(self.sortedNamesFileName, 'r')



    # upload json report files in folderPath to the database collection, create key josn file and hash file also
    def insert(self, folderPath):
        for subdir, dirs, files in os.walk(folderPath):
            for file in files:
                if file.endswith(".DS_Store") == False:
                    if file.endswith(".json"):
                        reportFilePath = os.path.join(subdir, file)
                        self.uploadReportList.append(reportFilePath)
                        command = "mongoimport --db " + self.databaseName + " --collection " +self.collectionName + " --file " + reportFilePath
                        subprocess.Popen(command, shell=True)
        self.queryClassToMakeKeys.connect()
        self.queryClassToMakeKeys.checkColumns()
        self.queryClassToMakeKeys.saveMykesToFile()
        self.getSortedNamesFile()
        self.createClassificationCollection()


    # copy new malware samples to the report directory
    # insert new reports to the collection of the database
    # update key and hash json files
    def copyNewSampleDirectory(self, source, destination, symlinks=False, ignore=None):
        newSource = source.split("/")[len(source.split("/"))-1]
        newDirectory = destination + "/" + newSource
        if not os.path.exists(newDirectory):
            os.makedirs(newDirectory)
        for item in os.listdir(source):
            subSource = os.path.join(source, item)
            subDestination = os.path.join(newDirectory, item)
            if os.path.isdir(subSource) == True:
                self.copyNewSampleDirectory(subSource, subDestination, symlinks, ignore)
            else:
                if not os.path.exists(subDestination) or os.stat(subSource).st_mtime - os.stat(
                        subDestination).st_mtime > 1:
                    shutil.copy2(subSource, subDestination)
        self.insert(source)
        self.hashFile.getSampleHashes()
        self.queryClassToMakeKeys.connect()
        self.queryClassToMakeKeys.checkColumns()
        self.queryClassToMakeKeys.saveMykesToFile()
        self.getSortedNamesFile()
        self.createClassificationCollection()


def main():
    myDB = CreateMongoDBandUploadReports("virustotal", "malware2018", "./category_2018_backup")
    myDB.connect()
    myDB.insert("./category_2018_backup")
    # myDB.copyNewSampleDirectory(src, sampleReporttDirectory)




if __name__ == '__main__':
    main()
