import os
import operator
import json
import sys


class GetFileTypeFrequency17000:

    def __init__(self, databaseName, collectionName, jsonReportDirectory):
        self.databaseName = databaseName
        self.collectionName = collectionName
        self.negativeTexeFile = self.databaseName + "_" + self.collectionName + "_negative.txt"
        self.firstdate = None
        self.jsonReportDirectory = jsonReportDirectory
        self.totalHashes = []
        self.malwareType = {}
        self.wholeType = []
        self.typeFrequency = {}


    def getFirstDate(self):
        directories = os.listdir(self.jsonReportDirectory)
        directories.sort()
        self.firstdate = directories[1]


    def readNegativeTextFile(self):
        self.negativeTexeFile = open(self.negativeTexeFile, "r")
        count = 0
        for line in self.negativeTexeFile:
            lineSplit = line.split("'")
            hash = lineSplit[1]
            self.totalHashes.append(hash)
            count += 1
            if count == 17000:
                break


    def getMalwareType(self):
        count = 1
        for hash in self.totalHashes:
            self.getMalwareTypeByHash(hash)
            print(str(count) + " malware get type")
            count += 1


    def getMalwareTypeByHash(self, hash):
        filePath = self.jsonReportDirectory + "/" + self.firstdate + "/" + hash + ".json"
        if os.path.isfile(filePath) == True:
            jsonFileOpen = open(filePath, "r")
            jsonFile = json.load(jsonFileOpen)
            typeResult = jsonFile['type']
            self.malwareType[hash] = typeResult
        else:
            print("there is no json file : " + hash)
            sys.exit(1)



    def getWholeType(self):
        count = 1
        for hash in self.malwareType:
            type = self.malwareType[hash]
            if type not in self.wholeType:
                self.wholeType.append(type)
            print(str(count) + " get type")
            count += 1


    def getTypeFrequency(self):
        count = 1
        for eachType in self.wholeType:
            result = 0
            for hash in self.malwareType:
                if self.checkContainingType(hash, eachType) == True:
                    result += 1
            self.typeFrequency[eachType] = result
            print(str(count) + " type checked")
            print("total type : " + str(len(self.wholeType)))
            count += 1



    def checkContainingType(self, hash, eachType):
        type = self.malwareType[hash]
        if type == eachType:
            return True


    def sortTypeFrequency(self):
        self.typeFrequency = sorted(self.typeFrequency.items(), key=operator.itemgetter(1), reverse=True)


    def saveTypeFrequency(self):
        fileName = "Type_Frequency17000_result.txt"
        filePath = "./" + fileName
        fileOpen = open(filePath, "w")
        for each in self.typeFrequency:
            fileOpen.write(str(each )+ "\n")





def main():
    myFrequency = GetFileTypeFrequency17000("virustotal", "malware2018", "json_reports")
    myFrequency.getFirstDate()
    myFrequency.readNegativeTextFile()
    myFrequency.getMalwareType()
    myFrequency.getWholeType()
    myFrequency.getTypeFrequency()
    myFrequency.sortTypeFrequency()
    myFrequency.saveTypeFrequency()


if __name__ == '__main__':
    main()
