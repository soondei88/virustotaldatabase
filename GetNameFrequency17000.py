import os
import operator
import json
import sys


class GetNameFrequency17000:

    def __init__(self, databaseName, collectionName, jsonReportDirectory):
        self.databaseName = databaseName
        self.collectionName = collectionName
        self.negativeTexeFile = self.databaseName + "_" + self.collectionName + "_negative.txt"
        self.firstdate = None
        self.jsonReportDirectory = jsonReportDirectory
        self.totalHashes = []
        self.malwareNames = {}
        self.wholeKeyword = []
        self.keywordFrequency = {}


    def getFirstDate(self):
        directories = os.listdir(self.jsonReportDirectory)
        directories.sort()
        self.firstdate = directories[1]


    def readNegativeTextFile(self):
        self.negativeTexeFile = open(self.negativeTexeFile, "r")
        count = 0
        for line in self.negativeTexeFile:
            lineSplit = line.split("'")
            hash = lineSplit[1]
            self.totalHashes.append(hash)
            count += 1
            if count == 17000:
                break


    def getMalwareNames(self):
        count = 1
        for hash in self.totalHashes:
            self.getMalwareNameByHash(hash)
            print(str(count) + " malware get name")
            count += 1


    def getMalwareNameByHash(self, hash):
        filePath = self.jsonReportDirectory + "/" + self.firstdate + "/" + hash + ".json"
        if os.path.isfile(filePath) == True:
            jsonFileOpen = open(filePath, "r")
            jsonFile = json.load(jsonFileOpen)
            scanResult = jsonFile['scans']
            fileName = []
            for sc in scanResult:
                AVResult = scanResult[sc]
                name = AVResult['result']
                if name != None:
                    if name not in fileName:
                        fileName.append(name)
            self.malwareNames[hash] = fileName
        else:
            print("there is no json file : " + hash)
            sys.exit(1)


    def saveMalwareNames(self):
        fileName = "Frequency17000_result.json"
        filePath = "./" + fileName
        fileOpen = open(filePath, "w")
        json.dump(self.malwareNames, fileOpen)
        print(filePath + " has been created")


    def getWholeKeyword(self):
        count = 1
        for hash in self.malwareNames:
            nameList = self.malwareNames[hash]
            for name in nameList:
                nameSplit1 = name.split(".")
                nameSplit2 = name.split(":")
                nameSplit3 = name.split("/")
                for eachName1 in nameSplit1:
                    if eachName1 not in self.wholeKeyword:
                        self.wholeKeyword.append(eachName1)
                for eachName2 in nameSplit2:
                    if eachName2 not in self.wholeKeyword:
                        self.wholeKeyword.append(eachName2)
                for eachName3 in nameSplit3:
                    if eachName3 not in self.wholeKeyword:
                        self.wholeKeyword.append(eachName3)
            print(str(count) + " get keyword")
            count += 1


    def getKeywordFrequency(self):
        count = 1
        for eachKeyword in self.wholeKeyword:
            result = 0
            for hash in self.malwareNames:
                if self.checkContainingKeyword(hash, eachKeyword) == True:
                    result += 1
            self.keywordFrequency[eachKeyword] = result
            print(str(count) + " keyword checked")
            print("total keyword : " + str(len(self.wholeKeyword)))
            count += 1



    def checkContainingKeyword(self, hash, keyword):
        nameList = self.malwareNames[hash]
        for name in nameList:
            if keyword in name:
                return True
        return False


    def sortKeywordFrequency(self):
        self.keywordFrequency = sorted(self.keywordFrequency.items(), key=operator.itemgetter(1), reverse=True)


    def saveKeywordFrequency(self):
        fileName = "Frequency17000_result.txt"
        filePath = "./" + fileName
        fileOpen = open(filePath, "w")
        for each in self.keywordFrequency:
            fileOpen.write(str(each )+ "\n")





def main():
    myFrequency = GetNameFrequency17000("virustotal", "malware2018", "json_reports")
    myFrequency.getFirstDate()
    myFrequency.readNegativeTextFile()
    myFrequency.getMalwareNames()
    myFrequency.getWholeKeyword()
    myFrequency.getKeywordFrequency()
    myFrequency.sortKeywordFrequency()
    myFrequency.saveKeywordFrequency()


if __name__ == '__main__':
    main()
