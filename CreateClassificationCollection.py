import pymongo
import os
import sys
import json
import copy
import bson
from ClassifyDatabase import ClassifyDatabase

class CreateClassificationCollection:

    def __init__(self, databaseName, originalCollectionName, sampleReporttDirectory):
        self.databaseName = databaseName
        self.originalCollectionName = originalCollectionName
        self.newCollectionName = self.originalCollectionName + "CC"
        self.sortedNamejsonFile = self.databaseName + "_" + self.originalCollectionName + "_names_sorted.json"
        print(self.sortedNamejsonFile)
        self.myClient = None
        self.myDatabase = None
        self.myCollection = None
        self.malwareNameDict = None
        self.possibleKeyword = {}
        self.classifyDatabase = None
        self.sampleReporttDirectory = sampleReporttDirectory

    def connect(self):
        self.myClient = pymongo.MongoClient("mongodb://localhost:27017/")
        self.myDatabase = self.myClient[self.databaseName]
        self.myCollection = self.myDatabase[self.newCollectionName]
        try:
            if os.path.isfile(self.sortedNamejsonFile) == True:
                nameFile = open(self.sortedNamejsonFile, 'r')
                self.malwareNameDict = json.load(nameFile)
                print("name file load success")

            else:
                self.classifyDatabase = ClassifyDatabase(self.databaseName, self.collectionName, self.sampleReporttDirectory)
                self.classifyDatabase.connect()
                self.classifyDatabase.getScanResultKeys()
                self.classifyDatabase.getMalwareName()
                self.classifyDatabase.sortMalwareName()
                self.classifyDatabase.saveSortedNames()
                self.sortedNamesFile = open(self.sortedNamejsonFile, 'r')
                self.malwareNameDict = json.load(self.sortedNamesFile)
        except:
            print("error: no json name file")
            sys.exit(1)

    def getPossibleKeyword(self):
        for hash in self.malwareNameDict:
            keyworldList = self.malwareNameDict[hash]
            for keyword in keyworldList:
                candidateKeyword = keyword[0]
                self.possibleKeyword[candidateKeyword] = []

    def classifyHash(self):
        count = 1
        print("length : " + str(len(self.malwareNameDict)))
        for hash in self.malwareNameDict:
            nameList = self.malwareNameDict[hash]
            for name in nameList:
                inputName = name[0]
                if hash not in self.possibleKeyword[inputName]:
                    self.possibleKeyword[inputName].append(hash)
                    # print(str(count) + " putting name")
            print(str(count) + " hash classified")
            count += 1
            # if count == 10:
            #     break


    def inpuDocumentToCC(self):
        count = 1
        for each in self.possibleKeyword:
            if len(self.possibleKeyword[each]) > 1:
                newDict = {}
                newEach = each.replace(".", "%")
                newDict[newEach] = self.possibleKeyword[each]
                self.myCollection.insert_one(newDict)
                print(str(count) + " documents are input")
                count += 1



    # create json dictionary and upload to the DB
    def saveClassificationResult(self):
        count = 0
        for each in self.possibleKeyword:
            if len(self.possibleKeyword[each]) > 1:
                newDict = {}
                newDict[each] = self.possibleKeyword[each]
                fileName = str(count) + "_CCresult.json"
                file = open(fileName, "w")
                json.dump(newDict, file)
                count += 1
                if count == 2:
                    break






def main():
    myCC = CreateClassificationCollection("virustotal", "malware2018", "./category_2018")
    myCC.connect()
    myCC.getPossibleKeyword()
    myCC.classifyHash()
    myCC.inpuDocumentToCC()
    # myCC.saveClassificationResult()


if __name__ == '__main__':
    main()