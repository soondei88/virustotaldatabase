import os
import pymongo
import json
import operator
import copy
from QueryMongoDB import QueryMongoDB


class ClassifyDatabase:

    def __init__(self, databaseName, collectionName, sampleReporttDirectory):
        self.databaseName = databaseName
        self.collectionName = collectionName
        self.myClient = None
        self.myDatabase = None
        self.myCollection = None
        self.sampleReporttDirectory = sampleReporttDirectory
        self.keyFile = None
        self.wholeDocument = None
        self.basicHash = 'sha256'
        self.scanResultKeys = []
        self.malwareName = None
        self.wholeName = []
        self.countNameOnce = {}
        self.countNameMultiple = {}
        self.negatives = {}

    # connect to the database, the collection
    def connect(self):
        self.myClient = pymongo.MongoClient("mongodb://localhost:27017/")
        self.myDatabase = self.myClient[self.databaseName]
        self.myCollection = self.myDatabase[self.collectionName]
        try:
            self.keyFile = open(self.databaseName + "_" + self.collectionName + "_keys.json", "r")
            self.keyFile = json.load(self.keyFile)
        except:
            QMDB = QueryMongoDB(self.databaseName, self.collectionName, self.sampleReporttDirectory)
            QMDB.connect()
            QMDB.checkColumns()
            self.keyFile = (QMDB.hashFileName, "r")
            self.keyFile = json.load(self.keyFile)
        self.wholeDocument = list(self.myCollection.find())

    def getScanResultKeys(self):
        scanResultKeys = []
        for eachKey in self.keyFile:
            if "scans" in eachKey:
                if "result" in eachKey:
                    scanResultKeys.append(eachKey)
                    # print(eachKey)
        self.scanResultKeys = scanResultKeys

    def getMalwareName(self):
        fileName = self.databaseName + "_" + self.collectionName + "_names_original.json"
        if os.path.isfile(fileName) == False:
            result = {}
            for eachDocument in self.wholeDocument:
                documentNameList = []
                for eachKey in self.scanResultKeys:
                    searchResult = self.getResultWithKey(eachKey, eachDocument)
                    if searchResult != None:
                        documentNameList.append(searchResult)
                if len(documentNameList) > 1:
                    result[eachDocument[self.basicHash]] = documentNameList
            self.malwareName = result
            resultNameFile = open(fileName, "w")
            json.dump(self.malwareName, resultNameFile)
        else:
            resultNameFile = open(fileName, "r")
            self.malwareName = json.load(resultNameFile)

    def getResultWithKey(self, searchKey, targetDict):
        keyList = searchKey.split()
        result = None
        try:
            count = 0
            for eachKey in keyList:
                if count == 0:
                    result = targetDict[eachKey]
                else:
                    result = result[eachKey]
                count += 1
            return result

        except:
            return None

    def showKeyAndName(self):
        for key in self.malwareName:
            print("key : " + key)
            print(self.malwareName[key])
            print("")
            print("")

    def searchName(self, keyWord):
        documentResult = []
        MatchingResult = []
        for eachMalware in self.malwareName:
            nameList = self.malwareName[eachMalware]
            for key in nameList:
                if keyWord.lower() in key.lower():
                    print(eachMalware + "\n " + "- " + key)
                    if key not in MatchingResult:
                        MatchingResult.append(key)
                    if eachMalware not in documentResult:
                        documentResult.append(eachMalware)
        print(str(len(documentResult)) + " samples")
        print(str(len(MatchingResult)) + " matchings")

    def sortMalwareName(self):
        countHigh = 0
        for eachMalware in self.malwareName:
            print(countHigh)
            print(eachMalware)
            countHigh += 1
            nameList = self.malwareName[eachMalware]
            resultDict1 = {}
            resultDict2 = {}
            resultDict3 = {}
            finalResult = []
            for name in nameList:
                nameSplitted1 = name.split(".")
                for ns1 in nameSplitted1:
                    if ns1 not in resultDict1:
                        resultDict1[ns1] = 1
                    else:
                        count1 = resultDict1[ns1]
                        resultDict1[ns1] = count1 + 1
                nameSplitted2 = name.split("/")
                for ns2 in nameSplitted2:
                    if ns2 not in resultDict2:
                        resultDict2[ns2] = 1
                    else:
                        count2 = resultDict2[ns2]
                        resultDict2[ns2] = count2 + 1
                nameSplitted3 = name.split(":")
                for ns3 in nameSplitted3:
                    if ns3 not in resultDict3:
                        resultDict3[ns3] = 1
                    else:
                        count2 = resultDict3[ns3]
                        resultDict3[ns3] = count2 + 1

            resultDict1 = sorted(resultDict1.items(), key=operator.itemgetter(1), reverse=True)
            resultDict2 = sorted(resultDict2.items(), key=operator.itemgetter(1), reverse=True)
            resultDict3 = sorted(resultDict3.items(), key=operator.itemgetter(1), reverse=True)

            for i in range(5):
                input1 = resultDict1[i]
                input2 = resultDict2[i]
                input3 = resultDict3[i]
                self.checkAppendCondition(finalResult, input1)
                self.checkAppendCondition(finalResult, input2)
                self.checkAppendCondition(finalResult, input3)

            self.malwareName[eachMalware] = finalResult
        self.sortMalwareNameAgain()

    def checkAppendCondition(self, targetList, inputTuple):
        if len(targetList) == 0:
            targetList.append(inputTuple)
        else:
            checkAppend = False
            for each in targetList:
                if each[0] == inputTuple[0]:
                    location = targetList.index(each)
                    newEach = targetList.pop(location)
                    originalNumber = newEach[1]
                    inputTupleNumber = inputTuple[1]
                    targetList.append((inputTuple[0], originalNumber + inputTupleNumber))
                    checkAppend = True
            if checkAppend == False:
                targetList.append(inputTuple)

    def sortMalwareNameAgain(self):
        count = 0
        for eachMalware in self.malwareName:
            print(count)
            print("sort agian:" + eachMalware)
            count += 1
            sortedEachMalware = sorted(self.malwareName[eachMalware], key=operator.itemgetter(1), reverse=True)
            self.malwareName[eachMalware] = sortedEachMalware


    def getWholeName(self):
        malwareNameCopy = copy.deepcopy(self.malwareName)
        for eachMaware in malwareNameCopy:
            nameList = self.malwareName[eachMaware]
            for element in nameList:
                name = element[0]
                if name not in self.wholeName:
                    self.wholeName.append(name)
        print("the number of the whole name : " + str(len(self.wholeName)))


    def getCountNameMultiple(self):
        malwareNameCopy = copy.deepcopy(self.malwareName)
        for eachMalware in malwareNameCopy:
            nameList = malwareNameCopy[eachMalware]
            for name in nameList:
                nameCharacter = name[0]
                nameCount = name[1]
                try:
                    originalCount = self.countNameMultiple[nameCharacter]
                    newCount = originalCount + nameCount
                    self.countNameMultiple[nameCharacter] = newCount
                except:
                    self.countNameMultiple[nameCharacter] = nameCount

    def getCountNameOnce(self):
        malwareNameCopy = copy.deepcopy(self.malwareName)
        for eachMalware in malwareNameCopy:
            nameList = malwareNameCopy[eachMalware]
            for name in nameList:
                nameCharacter = name[0]
                try:
                    originalCount = self.countNameOnce[nameCharacter]
                    newCount = originalCount + 1
                    self.countNameOnce[nameCharacter] = newCount
                except:
                    self.countNameOnce[nameCharacter] = 1

    def saveSortedNames(self):
        fileName1 = self.databaseName + "_" + self.collectionName + "_names_sorted.txt"
        fileName2 = self.databaseName + "_" + self.collectionName + "_names_sorted.json"
        resultNameFile1 = open(fileName1, "w")
        resultNameFile2 = open(fileName2, "w")
        count = 0
        for eachMalware in self.malwareName:
            print(count)
            print("save names : " + eachMalware)
            count += 1
            resultNameFile1.write(eachMalware + " : \n" + " - " + str(self.malwareName[eachMalware]) + "\n")
        json.dump(self.malwareName, resultNameFile2)

    def saveCountNameMultiple(self):
        fileName1 = self.databaseName + "_" + self.collectionName + "_names_count_multiple.txt"
        resultNameFile1 = open(fileName1, "w")
        count = 0
        self.countNameMultiple = sorted(self.countNameMultiple.items(), key=operator.itemgetter(1), reverse=True)
        for name in self.countNameMultiple:
            print(count)
            print("save names multiple : " + str(name))
            resultNameFile1.write(str(name) + "\n")
            count += 1

    def saveCountNameOnce(self):
        fileName1 = self.databaseName + "_" + self.collectionName + "_names_count_once.txt"
        resultNameFile1 = open(fileName1, "w")
        count = 0
        self.countNameOnce = sorted(self.countNameOnce.items(), key=operator.itemgetter(1), reverse=True)
        for name in self.countNameOnce:
            print(count)
            print("save names once : " + str(name))
            resultNameFile1.write(str(name) + "\n")
            count += 1


    def getNegative(self):
        for document in self.wholeDocument:
            hash = document[self.basicHash]
            total = document['total']
            print("total : " + str(total))
            positives = document['positives']
            print("positives : " + str(positives))
            negatives = total - positives
            # input = ("total : " + str(total), "positives : " + str(positives), "negatives : " + str(negatives))
            self.negatives[hash] = negatives
            # self.negatives[hash] = input



    def saveNegative(self):
        fileName1 = self.databaseName + "_" + self.collectionName + "_negative.txt"
        resultNameFile1 = open(fileName1, "w")
        self.negatives = sorted(self.negatives.items(), key=operator.itemgetter(1), reverse=True)
        for name in self.negatives:
            resultNameFile1.write(str(name) + "\n")
        # fileName1 = self.databaseName + "_" + self.collectionName + "_negative.json"
        # resultNameFile1 = open(fileName1, "w")
        # self.negatives = sorted(self.negatives.items(), key=operator.itemgetter(1), reverse=True)
        # json.dump(self.negatives, resultNameFile1)




    def makeIndexes(self):
        self.myCollection.create_index([("$**", pymongo.TEXT)])

    def searchUsingIndexes(self, keyword):
        result = list(self.myCollection.find({"$text": {"$search": keyword}}))
        for each in result:
            print(each['sha256'] + " : \n" + " - " + " type : " + each["type"])


def main():
    myDB = ClassifyDatabase("virustotal", "malware2018", "./category_2018_backup")
    myDB.connect()
    myDB.getScanResultKeys()
    myDB.getMalwareName()
    myDB.sortMalwareName()
    myDB.saveSortedNames()
    # myDB.getWholeName()
    # myDB.getCountNameOnce()
    # myDB.saveCountNameOnce()
    # myDB.getCountNameMultiple()
    # myDB.saveCountNameMultiple()
    # myDB.getNegative()
    # myDB.saveNegative()


if __name__ == '__main__':
    main()