import os
import pymongo
import shutil
import json
import datetime
import copy
import sys
import tarfile
import argparse
from ComputeHashesOfSamples import ComputeHashesOfSamples
import argparse

parser = argparse.ArgumentParser(description = 'Search using keyword(s) to find binaries of malware from the database')
parser.add_argument('-k', '--keyword', help = "keyword to search", required = True, action = 'append', dest="keyword")
parser.add_argument('-s', '--same', help = "search malware which is the exact same as the keyword, if not specified, False, find malware continas the keyword(s)", action='store_true', dest="same")
parser.add_argument('-t', '--text', help = "retrieve result as a txt file", action='store_true', dest="text")
parser.add_argument('-f', '--file', help = "retrieve result as an archive file", action='store_true', dest="file")
args = parser.parse_args()


class QueryClassificationCollection:

    def __init__(self, databaseName, originalCollectionName, sampleReporttDirectory):
        self.databaseName = databaseName
        self.originalCollectionName = originalCollectionName
        self.newCollectionName = self.originalCollectionName + "CC"
        self.myClient = None
        self.myDatabase = None
        self.myCollection = None
        self.sampleReporttDirectory = sampleReporttDirectory
        self.wholeDocument = None
        self.keyword = []
        self.searchKeyword = ""
        self.searchKeywordList = []
        self.multipleKeywords = False
        self.relevantSearch = None
        self.singleSearchResult = None
        self.multipleSearchResult = {}
        self.totalMultipleSearchNumber = 0
        self.currentTime = None
        self.queryResultDirectory = None
        self.dataHashFileName = self.databaseName+"_"+self.originalCollectionName+"_hash.json"
        self.endProgram = False
        try:
            self.dataHashFile = open(self.dataHashFileName, "r")
            self.dataHashFile = json.load(self.dataHashFileName)
        except:
            self.dataHashFile = ComputeHashesOfSamples(self.sampleReporttDirectory, self.databaseName + "_" + self.originalCollectionName + "_hash.json")
            self.dataHashFile.getSampleHashes()
            self.dataHashFile = open(self.dataHashFileName, "r")
            self.dataHashFile = json.load(self.dataHashFile)


    def connect(self):
        print("tryng to connect to the database")
        self.myClient = pymongo.MongoClient("mongodb://localhost:27017/")
        self.myDatabase = self.myClient[self.databaseName]
        self.myCollection = self.myDatabase[self.newCollectionName]
        self.wholeDocument = list(self.myCollection.find())


    def getKeyword(self):
        for eachDocument in self.wholeDocument:
            for keyword in eachDocument:
                if keyword not in self.keyword:
                    self.keyword.append(keyword)
        print("")
        print("")
        print("***** Welcome to DB : " + self.databaseName + " / Collection : " + self.newCollectionName + " *****")
        print("")
        print("")
        print(" - This program is for searching a list of malware based on keyword you input")
        print(" - This program search every malware which contains the keyword in their Virustotal scanning result")
        print(" - for example, the keyword can be trojan or HTML")
        print(" - You can retrive the result in a txt file or a compressed file")
        print(" - You can input multiple keywords")
        print(" - please do 'python QueryClassificationCollection2.py --hlep' for more information")


    def searchStart(self):
        self.setSearchKeyword()
        if self.multipleKeywords == False:
            self.startSearch()
        else:
            self.startMultipleSearch()



    def setSearchKeyword(self):
        print("")
        print("")
        print("***** Start Search!! *****")
        print("")
        print("")

        searchKeyword = args.keyword
        if len(searchKeyword) == 1:
            self.searchKeyword = searchKeyword[0]
        if len(searchKeyword) > 1:
            self.searchKeywordList = searchKeyword
            self.multipleKeywords = True

        if self.multipleKeywords == False:
            decision = args.same
            if decision == False:
                self.relevantSearch = True
            elif decision == True:
                self.relevantSearch = False


    def checkSearchKeywordList(self, keys):
        result = 0
        for keyword in self.searchKeywordList:
            if keyword.lower() in keys[0].lower().replace("%", "."):
                result += 1
        if result == len(self.searchKeywordList):
            return True
        else:
            return False



    def startMultipleSearch(self):
        for document in self.wholeDocument:
            keys = document.keys()
            if self.checkSearchKeywordList(keys) == True:
                self.multipleSearchResult[keys[0]] = document[keys[0]]
        if len(self.multipleSearchResult) > 0:
            print("found search result")
            self.getMultipleSearchResult()
        else:
            print("found no search result")



    def startSearch(self):
        if self.relevantSearch == False:
            for document in self.wholeDocument:
                keys = document.keys()
                if keys[0].lower().replace("%", ".") == self.searchKeyword.lower():
                    result = document[keys[0]]
                    self.singleSearchResult = result
                    print("found search result")
                    self.getSingleSearchResult()
                    break
                else:
                    print("found no search result")

        if self.relevantSearch == True:
            for document in self.wholeDocument:
                keys = document.keys()
                if self.searchKeyword.lower() in keys[0].lower().replace("%", "."):
                    # self.multipleSearchResult.append(document[keys[1]])
                    self.multipleSearchResult[keys[0]] = document[keys[0]]
            if len(self.multipleSearchResult) > 0:
                print("found search result")
                self.getMultipleSearchResult()
            else:
                print("found no search result")


    def getSingleSearchResult(self):
        numberOfHashes = len(self.singleSearchResult)
        print("search keyword : " + self.searchKeyword)
        print("found hashes : " + str(numberOfHashes))
        for each in self.singleSearchResult:
            print(" - " + each)
        print("found hashes : " + str(numberOfHashes))
        self.retrieveSingleResultText()
        self.retrieveSingleResultFile()


    def generateQueryResult(self, hashValue, destinationDirectory):
        sampleFilePath = self.dataHashFile[hashValue]
        shutil.copy(sampleFilePath, destinationDirectory)


    def compressQueryResult(self):
        fileName = self.queryResultDirectory.replace(":", "_")
        with tarfile.open(fileName+".tar.gz", "w:gz") as tar:
            tar.add(self.queryResultDirectory)
            tar.close()
        shutil.rmtree(self.queryResultDirectory)
        print(fileName + ".tar.gz" + " has been generated")


    def retrieveSingleResultFile(self):
        print("retrieving files")
        self.currentTime = str(datetime.datetime.now()).replace(" ", "_").replace("-", "_")
        self.queryResultDirectory = "./result-" + self.currentTime

        decision = args.file
        if decision ==  True:
            os.mkdir(self.queryResultDirectory)
            for each in self.singleSearchResult:
                each = each.replace("%", ".")
                self.generateQueryResult(each, self.queryResultDirectory)
            self.compressQueryResult()



    def retrieveSingleResultText(self):
        decision = args.text
        if decision == True:
            name = self.searchKeyword
            self.currentTime = str(datetime.datetime.now()).replace(" ", "_").replace("-", "_")
            resultFileName = self.currentTime + "_" + name + "_single_search_result.txt"
            resultFile = open(resultFileName, 'w')
            resultFile.write(name + " : " + str(len(self.singleSearchResult)) + " samples" + "\n")
            for each in self.singleSearchResult:
                resultFile.write(" - " + each + "\n")
            print(resultFileName + " has been generated")


    def getMultipleSearchResult(self):
        relevantKeyNumber = len(self.multipleSearchResult)
        print("search keyword : " + self.searchKeyword)
        print("total found relevant keyword : " + str(relevantKeyNumber))
        print("")
        totalNumber = 0
        for each in self.multipleSearchResult:
            print(each)
            for hash in self.multipleSearchResult[each]:
                print(" - " + hash)
            print(str(len(self.multipleSearchResult[each])) + "hashes")
            totalNumber = totalNumber + (len(self.multipleSearchResult[each]))
            print("")
        print("search keyword : " + self.searchKeyword)
        print("total found relevant keyword : " + str(relevantKeyNumber))
        print("total total hashes : " + str(totalNumber))
        self.totalMultipleSearchNumber = totalNumber
        self.retrieveMultipleResultText()
        self.retrieveMultipleResultFile()


    def retrieveMultipleResultFile(self):
        print("retrieving files")
        self.currentTime = str(datetime.datetime.now()).replace(" ", "_").replace("-", "_").replace(".", "_").replace(":", "_")
        self.queryResultDirectory = "./result_" + self.currentTime

        decision = decision = args.file
        if decision == True:
            os.mkdir(self.queryResultDirectory)
            for each in self.multipleSearchResult:
                neweach = copy.deepcopy(each)
                neweach.replace("%", ".")
                neweachSplit = neweach.split("/")
                subQueryResultDirectory = self.queryResultDirectory
                if len(neweachSplit) > 1:
                    for splited in neweachSplit:
                        subQueryResultDirectory =  subQueryResultDirectory + "/" + splited
                        if os.path.isdir(subQueryResultDirectory) == False:
                            os.mkdir(subQueryResultDirectory)
                else:
                    subQueryResultDirectory = self.queryResultDirectory + "/" + neweach
                    os.mkdir(subQueryResultDirectory)
                hashList = self.multipleSearchResult[each]
                for eachHash in hashList:
                    self.generateQueryResult(eachHash, subQueryResultDirectory)
            self.compressQueryResult()


    def retrieveMultipleResultText(self):
        decision = decision = args.text
        if decision == True:
            name = self.searchKeyword
            self.currentTime = str(datetime.datetime.now()).replace(" ", "_").replace("-", "_")
            resultFileName = self.currentTime + "_" + name + "_multiple_search_result.txt"
            resultFile = open(resultFileName, 'w')
            resultFile.write(name + " : " + str(len(self.multipleSearchResult)) + " total keywords" + "\n")
            resultFile.write(name + " : " + str(self.totalMultipleSearchNumber) + " total samples" + "\n")
            for keyword in self.multipleSearchResult:
                resultFile.write("\n")
                hashList = self.multipleSearchResult[keyword]
                hashListLength = len(hashList)
                resultFile.write(keyword.replace("%", ".") + " : " + str(hashListLength) + " samples" + "\n")
                for each in hashList:
                    resultFile.write(" - " + each + "\n")
            print(resultFileName + " has been generated")









def main():
    myCC = QueryClassificationCollection("virustotal", "malwareTotal", "strage")
    myCC.connect()
    myCC.getKeyword()
    myCC.searchStart()


if __name__ == '__main__':
    main()
