import os
import json
import hashlib


class ComputeHashesOfSamples:


    def __init__(self, sampleReporttDirectory, hashFileName):
        self.sampleReporttDirectory = sampleReporttDirectory
        self.hashFileName = hashFileName
        self.sampleHashes = {}

    def getSampleHashes(self):
        if os.path.isfile(self.hashFileName)  == False:
            resultHashFile = open(self.hashFileName, "w")
            print("no hash file, let's make it")
            count = 0
            for subdir, dirs, files in os.walk(self.sampleReporttDirectory):
                for file in files:
                    if file.endswith(".DS_Store") == False:
                        if file.endswith(".json") == False:
                            filePath = os.path.join(subdir, file)
                            hash = self.getHash(filePath)
                            self.sampleHashes[hash] = filePath
                            print(str(count) + " hashes")
                            count += 1
            json.dump(self.sampleHashes, resultHashFile)
        else:
            print("hash file already exist, let's use it")
            resultHashFile = open(self.hashFileName, "r")
            originalHashDictionary = json.load(resultHashFile)
            resultHashFile.close()
            resultHashFile = open(self.hashFileName, "w")
            for subdir, dirs, files in os.walk(self.sampleReporttDirectory):
                for file in files:
                    if file.endswith(".DS_Store") == False:
                        if file.endswith(".json") == False:
                            filePath = os.path.join(subdir, file)
                            hash = self.getHash(filePath)
                            originalHashDictionary[hash] = filePath
            self.sampleHashes = originalHashDictionary
            json.dump(self.sampleHashes, resultHashFile)



    def getHash(self, filename):
        hashObject = hashlib.sha256()
        with open(filename, 'rb') as file:
            chunk = 0
            while chunk != b'':
                chunk = file.read(1024)
                hashObject.update(chunk)
        return hashObject.hexdigest()
