import os
import sys
import json
import csv

class FindNegativeDifference:


    def __init__(self, databaseName, collectionName, jsonFileDirectory):
        self.databaseName = databaseName
        self.collectionName = collectionName
        self.jsonFileDirectory = jsonFileDirectory
        self.csvFile = self.databaseName + "_" + self.collectionName + "_" + "negative_result.csv"
        self.csvHeader = []
        self.totalNegativeResult = {}
        self.firstDayNegative = ""
        self.totalFirstDayScanResult = {}
        self.totalDifference = {}
        self.totalAVNameDifference = {}



    def readCSVFile(self):
        try:
            myCSVFile = open(self.csvFile, "r")
            count = 0
            for line in myCSVFile:
                if count == 0:
                    self.csvHeader = line
                else:
                    myCSVFile.close()
                    break
                count += 1
        except:
            print("An error occured while reading a CSV file, terminate this program")
            sys.exit(1)


    def getCSVHead(self):
        self.csvHeader = self.csvHeader.split(",")
        self.firstDayNegative = self.csvHeader[3]


    def processCVSFile(self):
        myCSVFile = open(self.csvFile, "r")
        count = 0
        for line in myCSVFile:
            if count > 0:
                self.makeLineToDict(line)
            count += 1


    def makeLineToDict(self, line):
        lineSplit = line.split(",")
        resultDict = {}
        count = 0
        for each in self.csvHeader:
            resultDict[each] = lineSplit[count]
            count += 1
        self.totalNegativeResult[lineSplit[0]] = resultDict


    def getTotalFirstDayScanResult(self):
        count = 1
        for key in self.totalNegativeResult.keys():
            print(count)
            print(key)
            self.getFirstDayScanResult(key)
            count += 1


    def getFirstDayScanResult(self, hash):
        if hash != "ad833f9e6ff95ba8902356a3d6e94004bfeaa373f7850bb091834008a0bcaeed":
            firstDate = self.firstDayNegative.replace("_negatives", "")
            jsonFilePath = self.jsonFileDirectory + "/" + firstDate + "/" + hash + ".json"
            try:
                jsonFileToRead = open(jsonFilePath, "r")
                jsonFile = json.load(jsonFileToRead)
                total = jsonFile['total']
                scans = jsonFile['scans']
                positives = jsonFile['positives']
                positiveResultName = []
                for AV in scans:
                    scanResult = scans[AV]
                    detecResult = scanResult['detected']
                    if detecResult == 1:
                        positiveResultName.append(AV)
                if positives == len(positiveResultName):
                    self.totalFirstDayScanResult[hash] = positiveResultName
                else:
                    print("Detected True resut is differen from positives")
                    print(hash)
                    sys.exit(1)
            except:
                print("there is no json file to search : " + hash)
                sys.exit(1)
        else:
            firstDate = self.csvHeader[6].replace("_negatives", "")
            jsonFilePath = self.jsonFileDirectory + "/" + firstDate + "/" + hash + ".json"
            try:
                jsonFileToRead = open(jsonFilePath, "r")
                jsonFile = json.load(jsonFileToRead)
                total = jsonFile['total']
                scans = jsonFile['scans']
                positives = jsonFile['positives']
                positiveResultName = []
                for AV in scans:
                    scanResult = scans[AV]
                    detecResult = scanResult['detected']
                    if detecResult == 1:
                        positiveResultName.append(AV)
                if positives == len(positiveResultName):
                    self.totalFirstDayScanResult[hash] = positiveResultName
                else:
                    print("Detected True resut is differen from positives")
                    print(hash)
                    sys.exit(1)
            except:
                print("there is no json file to search : " + hash)
                sys.exit(1)


    def getTotalDifference(self):
        for key in self.totalNegativeResult.keys():
            self.findDifference(key)


    def findDifference(self, hash):
        if hash != "ad833f9e6ff95ba8902356a3d6e94004bfeaa373f7850bb091834008a0bcaeed":
            record = self.totalNegativeResult[hash]
            standardNegative = record[self.firstDayNegative]
            dateDifference = []
            for each in self.csvHeader:
                if "negatives" in each :
                    if each != self.firstDayNegative:
                        toCompare = record[each]
                        toCompare = toCompare.replace("\n", "")
                        toCompare = toCompare.replace("\r", "")
                        if standardNegative != toCompare:
                            dateDifference.append(each)
            if len(dateDifference) > 0:
                self.totalDifference[hash] = dateDifference
        else:
            record = self.totalNegativeResult[hash]
            secondDay = self.csvHeader[6]
            standardNegative = record[secondDay]
            dateDifference = []
            for each in self.csvHeader:
                if "negatives" in each :
                    if each != secondDay:
                        if each != self.firstDayNegative:
                            toCompare = record[each]
                            toCompare = toCompare.replace("\n", "")
                            toCompare = toCompare.replace("\r", "")
                            if standardNegative != toCompare:
                                dateDifference.append(each)
            if len(dateDifference) > 0:
                self.totalDifference[hash] = dateDifference


    def getTotalAVNameDifference(self):
        for each in self.totalDifference:
            self.getAVNameFromDifference(each)


    def getAVNameFromDifference(self, hash):
        if hash != "ad833f9e6ff95ba8902356a3d6e94004bfeaa373f7850bb091834008a0bcaeed":
            firstdayResult = self.totalFirstDayScanResult[hash]
            differenceResult = self.totalDifference[hash]
            resultIncrease = {}
            resultDecrease = {}
            for eachDay in differenceResult:
                resultIncrease[eachDay] = []
                resultDecrease[eachDay] = []
                positiveResultName = []
                date = eachDay.replace("_negatives", "")
                date = date.replace("\r", "")
                date = date.replace("\n", "")
                fileLocation = self.jsonFileDirectory + "/" + date + "/" + hash + ".json"
                jsonFileOpen = open(fileLocation, "r")
                jsonFile = json.load(jsonFileOpen)
                scans = jsonFile['scans']
                for AV in scans:
                    scanResult = scans[AV]
                    detecResult = scanResult['detected']
                    if detecResult == 1:
                        positiveResultName.append(AV)
                for eachAV in positiveResultName:
                    if eachAV not in firstdayResult:
                        resultIncrease[eachDay].append(eachAV)
                for eachAV in firstdayResult:
                    if eachAV not in positiveResultName:
                        resultDecrease[eachDay].append(eachAV)
                if resultIncrease[eachDay] == []:
                    resultIncrease.pop(eachDay)
                if resultDecrease[eachDay] == []:
                    resultDecrease.pop(eachDay)
            finalResultDict = {}
            if len(resultIncrease) > 0:
                finalResultDict["increase"] = resultIncrease
            if len(resultDecrease) > 0:
                finalResultDict["decrease"] = resultDecrease
            originalResult = self.totalFirstDayScanResult[hash]
            finalResultDict["original_positive"] = originalResult
            self.totalAVNameDifference[hash] = finalResultDict

        else:
            firstdayResult = self.totalFirstDayScanResult[hash]
            differenceResult = self.totalDifference[hash]
            resultIncrease = {}
            resultDecrease = {}
            for eachDay in differenceResult:
                eachDay = eachDay.replace("_negatives", "")
                resultIncrease[eachDay] = []
                resultDecrease[eachDay] = []
                positiveResultName = []
                date = eachDay.replace("_negatives", "")
                date = date.replace("\r", "")
                date = date.replace("\n", "")
                fileLocation = self.jsonFileDirectory + "/" + date + "/" + hash + ".json"
                jsonFileOpen = open(fileLocation, "r")
                jsonFile = json.load(jsonFileOpen)
                scans = jsonFile['scans']
                for AV in scans:
                    scanResult = scans[AV]
                    detecResult = scanResult['detected']
                    if detecResult == 1:
                        positiveResultName.append(AV)
                for eachAV in positiveResultName:
                    if eachAV not in firstdayResult:
                        resultIncrease[eachDay].append(eachAV)
                for eachAV in firstdayResult:
                    if eachAV not in positiveResultName:
                        resultDecrease[eachDay].append(eachAV)
                if resultIncrease[eachDay] == []:
                    resultIncrease.pop(eachDay)
                if resultDecrease[eachDay] == []:
                    resultDecrease.pop(eachDay)
            finalResultDict = {}
            if len(resultIncrease) > 0:
                finalResultDict["increase"] = resultIncrease
            if len(resultDecrease) > 0:
                finalResultDict["decrease"] = resultDecrease
            originalResult = self.totalFirstDayScanResult[hash]
            finalResultDict["original_positive"] = originalResult
            self.totalAVNameDifference[hash] = finalResultDict


    def saveTotalAVNameDifference(self):
        jsonFilePath = self.databaseName + "_" + self.collectionName + "_" + "negative_result_difference.json"
        jsonFile = open(jsonFilePath, "w")
        json.dump(self.totalAVNameDifference, jsonFile)
        print(jsonFilePath + " has been created")



def main():
    myDifference = FindNegativeDifference("virustotal", "malware2018", "./json_reports")
    myDifference.readCSVFile()
    myDifference.getCSVHead()
    myDifference.processCVSFile()
    myDifference.getTotalFirstDayScanResult()
    myDifference.getTotalDifference()
    myDifference.getTotalAVNameDifference()
    myDifference.saveTotalAVNameDifference()



if __name__ == '__main__':
    main()
