import os
import re
import json
import shutil
import datetime
import pymongo
from ComputeHashesOfSamples import ComputeHashesOfSamples


class QueryMongoDB:

    def __init__(self, databaseName, collectionName, sampleReporttDirectory):
        self.databaseName = databaseName
        self.collectionName = collectionName
        self.sampleReporttDirectory = sampleReporttDirectory
        self.myClient = None
        self.myDatabase = None
        self.basicHash = 'sha256'
        self.myCollection = None
        self.findOneResult = None
        self.myKeys = []
        self.queryField = []
        self.queryKeyword = ""
        self.matchedKey = []
        self.queryResult = None
        self.queryResultHashes = []
        self.hashFileName = None
        try:
            self.dataHashFile = open(self.databaseName+"_"+self.collectionName+"_hash.json", "r")
            self.dataHashFile = json.load(self.dataHashFile)
        except:
            self.dataHashFile = ComputeHashesOfSamples(self.sampleReporttDirectory, self.databaseName + "_" + self.collectionName + "_hash.json")
            self.dataHashFile = json.load(self.dataHashFile)
        print("Welcome to database : " + self.databaseName + ", collection : " + self.collectionName)
        print("--------------------------------")
        print("--------------------------------")
        print("--------------------------------")
        print("")

    # to be connected to the database
    def connect(self):
        # default : localhost, port 27017
        self.myClient = pymongo.MongoClient("mongodb://localhost:27017/")
        self.myDatabase = self.myClient[self.databaseName]
        self.myCollection = self.myDatabase[self.collectionName]

    # check keys are stored in json file
    # if limit is set, the time of finding keys can be very short
    def checkColumns(self):
        keyFile = self.databaseName+"_"+self.collectionName+"_keys.json"
        if os.path.isfile(keyFile) == False:
            self.findOneResult = list(self.myCollection.find(limit = 1000))
            # self.findOneResult = list(self.myCollection.find())
            for each in self.findOneResult:
                self.findAllKeys("", each)
            self.saveMykesToFile()
        else:
            openedkeyFile = open(keyFile, "r")
            self.myKeys = json.load(openedkeyFile)
            self.showKeys()

    # for the collection of 10000 documents,
    # if pe-resource-list is included, keys are more than 30,000
    # if pe-resource-list is excluded, keys are about 1,100
    def findAllKeys(self, parentKey, myDict):
        if isinstance(myDict, dict):
            for key in myDict.keys():
                if key != "pe-resource-list":
                    if parentKey == "":
                        toInput = key
                    else:
                        toInput = parentKey + " " + key
                    if isinstance(myDict[key], dict) == True:
                        self.findAllKeys(toInput, myDict[key])
                    if isinstance(myDict[key], list) == True:
                        for each in myDict[key]:
                            self.findAllKeys(toInput, each)
                    else:
                        if toInput not in self.myKeys:
                            self.myKeys.append(toInput)

    # save keys in myKeys to json file for futher porposes
    def saveMykesToFile(self):
        fileName = self.databaseName+"_"+self.collectionName+"_keys.json"
        self.hashFileName = fileName
        resultkeyFile = open(fileName, "w")
        json.dump(self.myKeys, resultkeyFile)

    # print all keys
    def showKeys(self):
        print("")
        print("database : " + self.databaseName + ", collection : " + self.collectionName + " has following columns : ")
        print("")
        self.myKeys.sort()
        for key in self.myKeys:
            print(key)

    # find field(key) matching keyword
    def findField(self, keyword):
        findResult = []
        for eachKey in self.myKeys:
            if keyword.lower() in eachKey.lower():
                findResult.append(eachKey)
        return findResult

    # find field(key) matching keyword with And condition
    def findFieldAndCondition(self, keyword, otherList):
        findResult = []
        for eachKey in self.myKeys:
            if keyword.lower() in eachKey.lower():
                if len(otherList) == 0:
                    findResult.append(eachKey)
                else:
                    if keyword in otherList:
                        findResult.append(eachKey)
        return findResult

    # find field(key) for superfind
    def superFindGetField(self):
        # find matched field
        while (True):
            print("")

            fieldLength = 0
            while(fieldLength == 0):
                field = raw_input("please enter the name or keword to find mathicng field or sub-field\nyou can enter multiple keywords : ")
                fieldLength = len(field)
                field = field.split()

            if len(field) > 1:
                print("")
                print("you entered multiple keywords")
                print("do you want to search field using 'and' condition / 'or' condition?")
                while (True):
                    condition = raw_input("Please type and for 'a' / or for 'o' : ")
                    print("")
                    if condition.lower() == 'a' or condition.lower() == 'o' :
                        break
                    else :
                        print("you entered a wrong character! ")
                        print("")

                if condition == 'o':
                    for keyword in field:
                        findResult = self.findField(keyword)
                        for eachResult in findResult:
                            if eachResult not in self.matchedKey:
                                self.matchedKey.append(eachResult)

                elif condition == 'a':
                    findResult = []
                    count = 0
                    for keyword in field:
                        tempFindResult = self.findField(keyword)
                        if count == 0:
                            findResult = tempFindResult
                        else:
                            findResult = list(set(findResult).intersection(set(tempFindResult)))
                        count += 1
                    self.matchedKey = findResult
            else:
                field = field[0]
                findResult = self.findField(field)
                self.matchedKey.extend(findResult)

            numberOfKey = len(self.matchedKey)
            if numberOfKey == 0:
                print("No mathicng or You made a mistake, wrong name or case sensitive")
                print("")

            if numberOfKey != 0:
                print("")
                for mk in self.matchedKey:
                    print(mk)
                print(str(numberOfKey) + " matched field found")
                print("")
                break


    # set the keyword for superfind and execute superfind
    def superFindExecute(self):
        searchResult = {}
        self.queryKeyword = raw_input("please enter the keyword to search : ")
        print("keyword : " + self.queryKeyword)

        for mk in self.matchedKey:
            splitedMk = mk.split()
            field = ""
            if len(splitedMk) == 1:
                field = mk
            else:
                for child in splitedMk:
                    field = field + "." + child
                field = field[1:]
            if self.queryKeyword == 'c++' or self.queryKeyword == 'C++':
                result = self.myCollection.find(filter={field: {'$regex': self.queryKeyword+'\+' + '\+', '$options': 'i'}})
            else:
                result = self.myCollection.find(filter={field: {'$regex': self.queryKeyword, '$options': 'i'}})
            tempResult = list(result)
            if len(tempResult) > 0:
                searchResult[field] = tempResult

        self.queryResult = searchResult
        # print(self.queryResult)

        if len(self.queryResult) >= 1:
            print("")
            for key in self.queryResult.keys():
                eachResult = self.queryResult[key]
                for document in eachResult:
                    try:
                        if len(key.split(".")) == 1:
                            documentResult = document[key]
                        else:
                            keyList = key.split(".")
                            count =  0
                            documentResult = None
                            for eachKey in keyList:
                                if count == 0:
                                    documentResult = document[eachKey]
                                else:
                                    documentResult = documentResult[eachKey]
                                count += 1
                        print("sample hash : " + document[self.basicHash] + " \n  - matched key : " + key + " / - matched part : " + documentResult)
                        if document[self.basicHash] not in self.queryResultHashes:
                            self.queryResultHashes.append(document[self.basicHash])
                    except:
                        print("")
            print("")
            print(str(len(self.queryResultHashes)) + " samples found")
            print("")
            self.getQueryResult()
        else:
            print("")
            print("query result : None")
            print("")


    # ask the user to want the samples of the result
    # if yes, create a new folder, copy samples from the storage folder and put the samples to to new folder
    # compress the new folder
    def getQueryResult(self):
        self.currentTime = str(datetime.datetime.now()).replace(" ", "_").replace("-","_")
        self.queryResultDirectory = "./result-"+self.currentTime
        while(True):
            searchFile = raw_input("do you want to get the files of the result? plese input y or n : ")
            # if searchFile == 'y' or "Y" or "yes" or "Yes" or "YES" :
            if searchFile == 'y' or searchFile == "Y" or searchFile == "yes" or searchFile == "Yes" or searchFile == "YES":
                os.mkdir(self.queryResultDirectory)
                for eachHash in self.queryResultHashes:
                    self.generateQueryResult(eachHash)
                self.compressQueryResult()
                break
            if searchFile == 'n' or searchFile == "N" or searchFile == "no" or searchFile == "No" or searchFile == "NO":
                break

    # copy the samples to the new folder
    def generateQueryResult(self, hashValue):
        sampleFilePath = self.dataHashFile[hashValue]
        shutil.copy(sampleFilePath, self.queryResultDirectory)


    # compress the new folder
    def compressQueryResult(self):
        shutil.make_archive(self.queryResultDirectory + ".zip", 'zip', self.queryResultDirectory)
        print("your query result is : " + self.queryResultDirectory + ".zip")
        shutil.rmtree(self.queryResultDirectory)





def main():
    myDB = QueryMongoDB("virustotal", "malware2018", "./category_2018_backup")
    myDB.connect()
    myDB.checkColumns()
    myDB.superFindGetField()
    myDB.superFindExecute()





if __name__ == '__main__':
    main()