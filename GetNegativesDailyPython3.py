import os
import sys
import json
import copy
import csv
import datetime
import time
import requests
import logging

logging.basicConfig(filename="GetNegativesDaily_Log.log", level=logging.DEBUG)

# save json file everyday result
# log file : how many files, date and number of json file


class GetNegaTivesDaily:

    def __init__(self, negativeTexeFile):
        self.negativeTexeFile = negativeTexeFile
        self.hashes20000 = []
        # academic apikey
        self.apiKey = '8abb46c75c8a09fd85c50cc9f1d5f1fe24736f676f4c03702ebe4f18680594d4'
        # soonwon key
        # self.apiKey = '26bb83a6a52f785fc36f0cebb154c54335dfa824d108bd8faa9d9b45a5dc53bb'
        self.csvFile = negativeTexeFile[:len(negativeTexeFile)-4] + "_result.csv"
        self.jsonReport = {}

    # have to change if count == 10: to 17000,
    # now it is a trial
    def openTextFile(self):
        try:
            self.negativeTexeFile = open(self.negativeTexeFile, 'r')
            count = 0
            for line in self.negativeTexeFile:
                lineSplit = line.split("'")
                hash = lineSplit[1]
                self.hashes20000.append(hash)
                count += 1
                logging.debug(self.getCurrentTime() + "openTextFile success : " + str(hash))
                if count == 17000:
                    break
        except:
            logging.debug(self.getCurrentTime() + "openTextFile failed " + "no text file")
            sys.exit(1)


    def getCurrentTime(self):
        return str(datetime.datetime.today()) + " "


    def getReport(self):
        count = 0
        for hash in self.hashes20000:
            print(str(count) + " : " + hash )
            count += 1
            params = {'apikey': self.apiKey, 'resource': hash, 'allinfo': "true"}
            headers = {"Accept-Encoding": "gzip, deflate",
                       "User-Agent": "gzip,  My Python requests library example client or username"}
            response = requests.get('https://www.virustotal.com/vtapi/v2/file/report', params=params, headers=headers)
            time.sleep(1)
            try:
                if response.status_code == 200:
                    report = response.json()
                    self.jsonReport[hash] = report
                    logging.debug(self.getCurrentTime() + "getReport success : " + str(hash))
                else :
                    logging.debug(self.getCurrentTime() + "getReport failed, invalid status code : " + str(hash))
            except:
                logging.debug(self.getCurrentTime() + "getReport failed, error : " + str(hash))


    def saveJsonFile(self):
        if os.path.isdir("./json_reports/") == False:
            os.mkdir("./json_reports/")

        for hash in self.jsonReport:
            try:
                report = self.jsonReport[hash]
                fileName = hash + ".json"
                today = str(datetime.datetime.today()).split()[0]
                folderPath = "./json_reports/" + today
                filePath = folderPath + "/" + fileName
                if os.path.isdir(folderPath) == False:
                    os.mkdir(folderPath)
                fileOpend = open(filePath, "w")
                json.dump(report, fileOpend)
                logging.debug(self.getCurrentTime() + "saveJsonFile success : " + str(hash))
            except:
                logging.debug(self.getCurrentTime() + "saveJsonFile failed : " + str(hash))

            time.sleep(0.5)



    def setCSVFile(self):
        if os.path.isfile(self.csvFile) == False:
            myCSVFile = open(self.csvFile, 'w', newline='')
            fieldnames = ['malware hash']
            csvWriter = csv.DictWriter(myCSVFile, fieldnames=fieldnames)
            csvWriter.writeheader()
            for hash in self.hashes20000:
                csvWriter.writerow({'malware hash': hash})
            myCSVFile.close()
            logging.debug(self.getCurrentTime() + "setCSVFile success")
        else:
            logging.debug(self.getCurrentTime() + "setCSVFile success, but you have already CSVFile")


    def addHeadToCSVFile(self, newDay):
        myCSVFile = open(self.csvFile, newline='')
        oldName = 'output.csv'
        newName = str(self.csvFile)
        with open(oldName, 'w', newline='') as newCSVFile:
            originalFile = csv.reader(myCSVFile)
            # this is for python2
            # originalHead = originalFile.next()
            # this is for python3
            originalHead = next(originalFile)
            newHead = [newDay+"_total", newDay+"_positives", newDay+"_negatives"]
            for column in newHead:
                originalHead.append(column)
            newCSVFileWriter = csv.DictWriter(newCSVFile, fieldnames=originalHead)
            newCSVFileWriter.writeheader()

            count = 0
            for row in originalFile:
                print(count)
                inputDict = {}
                originalHeadLength = len(originalHead)
                for i in range(originalHeadLength-3):
                    inputDict[originalHead[i]] = row[i]
                for column in newHead:
                    inputKey = column[len(newDay)+1:]
                    if inputKey == 'total' or inputKey == 'positives':
                        try:
                            newvalue = self.jsonReport[row[0]][inputKey]
                            inputDict[column] = newvalue
                            logging.debug(self.getCurrentTime() + "addHeadToCSVFile success : " + column + str(row[0]) + " - " + str(inputKey))
                        except:
                            logging.debug(self.getCurrentTime() + "addHeadToCSVFile failed : " + column + str(row[0]) + " - " + str(inputKey))
                    elif inputKey == 'negatives':
                        try:
                            totalValue = self.jsonReport[row[0]]['total']
                            positivesValue = self.jsonReport[row[0]]['positives']
                            newvalue = totalValue - positivesValue
                            inputDict[column] = newvalue
                        except:
                            logging.debug(self.getCurrentTime() + "addHeadToCSVFile failed : " + column + str(row[0]) + " - " + str(inputKey))
                count += 1
                newCSVFileWriter.writerow(inputDict)

        newCSVFile.close()
        myCSVFile.close()
        os.rename(oldName, newName)




def main():
    myNegatives = GetNegaTivesDaily('virustotal_malware2018_negative.txt')
    myNegatives.openTextFile()
    myNegatives.setCSVFile()
    myNegatives.getReport()
    myNegatives.addHeadToCSVFile(str(datetime.datetime.today()).split()[0])
    myNegatives.saveJsonFile()


if __name__ == '__main__':
    main()