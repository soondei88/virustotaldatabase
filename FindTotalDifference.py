import os
import sys
import json
import csv

class FindTotalDifference:

    def __init__(self, databaseName, collectionName, jsonFileDirectory):
        self.databaseName = databaseName
        self.collectionName = collectionName
        self.jsonFileDirectory = jsonFileDirectory
        self.csvFile = self.databaseName + "_" + self.collectionName + "_" + "negative_result.csv"
        self.totalHashes = []
        self.reportDate = []
        self.firstDate = {}
        self.totalFirstDateResult = {}
        self.totalDifference = {}


    def readCSVFile(self):
        try:
            myCSVFile = open(self.csvFile, "r")
            count = 0
            for line in myCSVFile:
                if count > 0:
                    lineSplit = line.split(",")
                    self.totalHashes.append(lineSplit[0])
                    print(lineSplit[0])
                count += 1
                # if count == 10:
                #     break
        except:
            print("An error occured while reading a CSV file, terminate this program")
            sys.exit(1)


    def getReportDate(self):
        self.reportDate = os.listdir(self.jsonFileDirectory)
        self.reportDate.sort()
        # print(self.reportDate)


    def getTotalFirstDateResult(self):
        count = 0
        for hash in self.totalHashes:
            self.getFirstDateResult(hash)
            print(str(count) + " getFirstDateResult")
            count += 1
            # if count == 5:
            #     break


    def getFirstDateResult(self, hash):
        try:
            fileToOpen = None
            for eachDay in self.reportDate:
                filePath = self.jsonFileDirectory + "/" + eachDay + "/" + hash + ".json"
                if os.path.isfile(filePath) == True:
                    fileToOpen = open(filePath, "r")
                    self.firstDate[hash] = eachDay
                    break
            jsonFile = json.load(fileToOpen)
            totalNumber = jsonFile["total"]
            totalAVName = []
            totalPositiveNumber = jsonFile["positives"]
            totalPositiveName = []
            totalScanResult = jsonFile["scans"]
            for AV in totalScanResult:
                totalAVName.append(AV)
                eachScanResult = totalScanResult[AV]
                detectResult = eachScanResult["detected"]
                if detectResult == 1:
                    totalPositiveName.append(AV)
            finalResult = {}
            finalResult["totalNumber"] = totalNumber
            finalResult["totalAVName"] = totalAVName
            finalResult["totalPositiveNumber"] = totalPositiveNumber
            finalResult["totalPositiveName"] = totalPositiveName
            self.totalFirstDateResult[hash] = finalResult

        except:
            print("Error on getFirstDateResult : " + hash)
            sys.exit(1)


    def getTotalDifference(self):
        count = 0
        for hash in self.totalHashes:
            self.getDifference(hash)
            print(str(count) + " getTotalDifference")
            count += 1
            # if count == 5:
            #     break


    def getDifference(self, hash):
        try:
            firstDate = self.firstDate[hash]
            differenceResult = {}
            for eachDay in self.reportDate:
                if eachDay != firstDate:
                    filePath = self.jsonFileDirectory + "/" + eachDay + "/" + hash + ".json"
                    if os.path.isfile(filePath) == True:
                        fileToOpen = open(filePath, "r")
                        jsonFile = json.load(fileToOpen)
                        newTotalNumber = jsonFile["total"]
                        newTotalAVName = []
                        newTotalPositiveNumber = jsonFile["positives"]
                        newTotalPositiveName = []
                        newTotalScanResult = jsonFile["scans"]
                        for AV in newTotalScanResult:
                            newTotalAVName.append(AV)
                            eachScanResult = newTotalScanResult[AV]
                            detectResult = eachScanResult["detected"]
                            if detectResult == 1:
                                newTotalPositiveName.append(AV)
                        originalTotalNumber = self.totalFirstDateResult[hash]["totalNumber"]
                        originalTotalAVName = self.totalFirstDateResult[hash]["totalAVName"]
                        originalTotalPositiveNumber = self.totalFirstDateResult[hash]["totalPositiveNumber"]
                        originalTotalPositiveName = self.totalFirstDateResult[hash]["totalPositiveName"]

                        dayResult = {}
                        if originalTotalNumber == newTotalNumber:
                            if set(originalTotalAVName) != set(newTotalAVName):
                                increase = list(set(newTotalAVName) & (set(originalTotalAVName) ^ set(newTotalAVName)))
                                decrease = list(set(originalTotalAVName) & (set(originalTotalAVName) ^ set(newTotalAVName)))
                                dayResult["total"] = {}
                                dayResult["total"]["total_number"] = newTotalNumber
                                if len(increase) > 0 or len(decrease) > 0 :
                                    dayResult["total"]["same"] = {}
                                    dayResult["total"]["same"]["increase"] = increase
                                    dayResult["total"]["same"]["decrease"] = decrease
                        elif originalTotalNumber > newTotalNumber:
                            increase = list(set(newTotalAVName) & (set(originalTotalAVName) ^ set(newTotalAVName)))
                            decrease = list(set(originalTotalAVName) & (set(originalTotalAVName) ^ set(newTotalAVName)))
                            dayResult["total"] = {}
                            dayResult["total"]["total_number"] = newTotalNumber
                            if len(increase) > 0 or len(decrease) > 0:
                                dayResult["total"]["down"] = {}
                                dayResult["total"]["down"]["increase"] = increase
                                dayResult["total"]["down"]["decrease"] = decrease

                        elif originalTotalNumber < newTotalNumber:
                            increase = list(set(newTotalAVName) & (set(originalTotalAVName) ^ set(newTotalAVName)))
                            decrease = list(set(originalTotalAVName) & (set(originalTotalAVName) ^ set(newTotalAVName)))
                            dayResult["total"] = {}
                            dayResult["total"]["total_number"] = newTotalNumber
                            if len(increase) > 0 or len(decrease) > 0:
                                dayResult["total"]["up"] = {}
                                dayResult["total"]["up"]["increase"] = increase
                                dayResult["total"]["up"]["decrease"] = decrease


                        if originalTotalPositiveNumber == newTotalPositiveNumber:
                            if set(originalTotalPositiveName) != set(newTotalPositiveName):
                                increase = list(set(newTotalPositiveName) & (set(originalTotalPositiveName) ^ set(newTotalPositiveName)))
                                decrease = list(set(originalTotalPositiveName) & (set(originalTotalPositiveName) ^ set(newTotalPositiveName)))
                                dayResult["positives"] = {}
                                dayResult["positives"]["positives_number"] = newTotalPositiveNumber
                                if len(increase) > 0 or len(decrease) > 0:
                                    dayResult["positives"]["same"] = {}
                                    dayResult["positives"]["same"]["increase"] = increase
                                    dayResult["positives"]["same"]["decrease"] = decrease

                            elif originalTotalPositiveNumber > newTotalPositiveNumber:
                                increase = list(set(newTotalPositiveName) & (set(originalTotalPositiveName) ^ set(newTotalPositiveName)))
                                decrease = list(set(originalTotalPositiveName) & (set(originalTotalPositiveName) ^ set(newTotalPositiveName)))
                                dayResult["positives"] = {}
                                dayResult["positives"]["positives_number"] = newTotalPositiveNumber
                                FinalResult["positives"]["decrease"] = decrease
                                if len(increase) > 0 or len(decrease) > 0:
                                    dayResult["positives"]["down"] = {}
                                    dayResult["positives"]["down"]["increase"] = increase
                                    dayResult["positives"]["down"]["decrease"] = decrease

                            elif originalTotalPositiveNumber < newTotalPositiveNumber:
                                increase = list(set(newTotalPositiveName) & (set(originalTotalPositiveName) ^ set(newTotalPositiveName)))
                                decrease = list(set(originalTotalPositiveName) & (set(originalTotalPositiveName) ^ set(newTotalPositiveName)))
                                dayResult["positives"] = {}
                                dayResult["positives"]["positives_number"] = newTotalPositiveNumber
                                if len(increase) > 0 or len(decrease) > 0:
                                    dayResult["positives"]["up"] = {}
                                    dayResult["positives"]["up"]["increase"] = increase
                                    dayResult["positives"]["up"]["decrease"] = decrease

                        if len(dayResult) > 0:
                            differenceResult[eachDay] = dayResult
            if len(differenceResult) > 0:
                original_totalAVName = self.totalFirstDateResult[hash]["totalAVName"]
                original_totalNumber = self.totalFirstDateResult[hash]["totalNumber"]
                original_PositiveName = self.totalFirstDateResult[hash]["totalPositiveName"]
                original_totalPositiveNumber = self.totalFirstDateResult[hash]["totalPositiveNumber"]
                differenceResult["original_totalName"] = original_totalAVName
                differenceResult["original_totalNumber"] = original_totalNumber
                differenceResult["original_PositiveName"] = original_PositiveName
                differenceResult["original_original_totalPositiveNumber"] = original_totalPositiveNumber
                self.totalDifference[hash] = differenceResult
        except:
            print("Error on getDifference : " + hash)
            sys.exit(1)




    def saveTotalDifference(self):
        fileName = self.databaseName + "_" + self.collectionName + "_" + "total_difference.json"
        fileToOpen = open(fileName, "w")
        json.dump(self.totalDifference, fileToOpen)








def main():
    myDifference = FindTotalDifference("virustotal", "malware2018", "./json_reports")
    myDifference.readCSVFile()
    myDifference.getReportDate()
    myDifference.getTotalFirstDateResult()
    myDifference.getTotalDifference()
    myDifference.saveTotalDifference()



if __name__ == '__main__':
    main()